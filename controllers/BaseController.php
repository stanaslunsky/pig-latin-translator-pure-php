<?php

class BaseController
{
    protected string $basePath = '/pig-latin-translator-pure-php/';

    /**
     * Run action.
     *
     * @param string $action
     * @return mixed
     */
    public function run(string $action)
    {
        if (!method_exists($this, $action)) {
            $action = 'showHomepage';
        }

        return $this->$action();
    }

    /**
     * Load default page layout.
     */
    protected function showHomepage()
    {
        $basePath = $this->basePath;

        include 'views/layout.php';
    }
}