$(window).ready(function () {

    /**
     * AJAX function on key up gradually translates user input to Pig Latin.
     **/
    $('#input').on('keyup', function () {

        let input = $('#input').val();
        let output = $('#output');
        let baseurl = window.location.origin + window.location.pathname;

        $.ajax({
            url: baseurl + 'index.php?controller=piglatintranslator&action=translate',
            method: 'POST',
            data: {input: input},
            success: function (data) {
                output.val(data);
            }
        });
    });
});

