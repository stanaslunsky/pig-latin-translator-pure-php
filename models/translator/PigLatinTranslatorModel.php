<?php

class PigLatinTranslatorModel
{
    protected PigLatinTranslatorRulesModel $rules;

    /**
     * PigLatinTranslatorModel constructor.
     */
    public function __construct()
    {
        $this->rules = new PigLatinTranslatorRulesModel();
    }

    /**
     * User input processing.
     *
     * @param string $userInput User text input to translate
     * @return string Pig Latin Translation
     */
    public function getPigTranslate(string $userInput): string
    {
        if (empty($userInput))
            return '';

        $userInput = strtolower($userInput);

        $words = explode(' ', $userInput);
        $pigTranslation = "";

        foreach ($words as $word) {
            $word = $this->translateWord($word);

            // In case of first symbol is not in english alphabet, do not add anything.
            if (!empty($word)) {
                $pigTranslation .= ' ' . $word;
            }
        }

        return ltrim($pigTranslation);
    }

    /**
     * Specify the following word processing method and returns it's translation.
     *
     * @param string $word Word
     * @return string Pig Latin Translation
     */
    private function translateWord(string $word): string
    {
        $specialChar = '';

        if ($this->rules->checkForSpecialCharacters($word)) {
            $specialChar = substr($word, -1);
            $word = substr_replace($word, '', -1);
        }

        if ($this->rules->checkVowelsFirst($word)) {
            return $word . 'yay' . $specialChar;

        } elseif ($this->rules->checkConsonantsFirst($word)) {
            return preg_replace('/^(y*[' . PigLatinTranslatorConstants::CONSONANTS . ']*)([' . PigLatinTranslatorConstants::VOWELS . 'y].*)$/', "$2$1ay", $word) . $specialChar;
        }

        return '';
    }
}